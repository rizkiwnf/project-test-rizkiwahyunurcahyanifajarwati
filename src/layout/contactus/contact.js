import userEvent from "@testing-library/user-event";
import { useState } from "react";
import Validation from "./validation";
import Home from "../homepage/home";


function Contact() {
    const [values, setValues] = useState({
        name: '',
        email: '',
        textarea: '',
    });

    const [errors, setErrors] = useState({})
    function handleInput(event) {
        const newObj = { ...values, [event.target.name]: event.target.value }
        setValues(newObj)
    }

    function handleValidation(event) {
        event.preventDefault();
        setErrors(Validation(values));
    }

    return (
        <div className="mt-[10%] mx-[30%] mb-20">
            <div className=" text-center font-roboto mb-8 font-extrabold text-xl uppercase tracking-loose">
                Contact Us
            </div>
            <form className="font-roboto" onSubmit={handleValidation}>
                <div class="mb-6">
                    <label for="name" className="block mb-2 text-sm font-medium text-gray-900">Name</label>
                    <input type="name" id="name" className={`form-control ${errors.name && "invalid"} bg-gray-50 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 capitalize`} name="name" placeholder="masukkan nama anda" onChange={handleInput} />
                    {errors.name && <p style={{ color : "red", fontSize: "12px"}}>{errors.name}</p>}
                </div>
                <div class="mb-6">
                    <label for="email" className="block mb-2 text-sm font-medium text-gray-900">Your email</label>
                    <input type="email" id="email" className={`form-control ${errors.email && "invalid"} bg-gray-50 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5`} name="email" placeholder="name@email.com" onChange={handleInput} />
                    {errors.email && <p style={{ color : "red", fontSize: "12px"}}>{errors.email}</p>}
                </div>
                <div className="mb-6">
                    <label for="message" class="block mb-2 text-sm font-medium text-gray-900">Your message</label>
                    <textarea id="message" rows="4" className={`form-control ${errors.textarea && "invalid"} block p-2.5 w-full text-sm text-gray-900 bg-gray-50 border border-gray-300 focus:ring-blue-500 focus:border-blue-500 capitalize`} name="textarea" placeholder="tuliskan pesan" onChange={handleInput} />
                    {errors.textarea && <p style={{ color : "red", fontSize: "12px"}}>{errors.textarea}</p>}
                </div>
                <button type="submit" className="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm w-full px-5 py-2.5 text-center ">Submit</button>
            </form>
        </div>

    );
}
export default Contact;