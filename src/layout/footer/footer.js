import React from "react";

const Footer = () => {
    return (
        <footer className="justify-center text-center">
            <div className="bg-black/80 py-3 text-xs">
                <div className="text-white font-medium">
                    Copyright @ 2016 PT Company
                </div>
                <ul className="py-1 flex justify-center items-center space-x-3">
                    <li className="w-4 h-4"><img src="./assets/footer/facebook.png" className="" alt="fb-icon"/></li>
                    <li className="w-4 h-4"><img src="./assets/footer/twitter.png" alt="tw-icon"/></li>
                </ul>
                
            </div>
        </footer>
    )
}
export default Footer;