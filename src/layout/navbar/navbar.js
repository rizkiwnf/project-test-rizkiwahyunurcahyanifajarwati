import { useState } from "react";

const Navbar = () => {
    const [open, setOpen] = useState(false);
    return (
        <header className="Navbar-section">
            <nav className="bg-white">
                <div className="flex items-center justify-around">
                    <div className="left-side text-black font-extrabold text-xl p-2 font-roboto">
                        Company
                    </div>
                    <div onMouseLeave={() => setOpen(false)} className="right-side">
                        <ul className="flex justify-between text-gray-400 uppercase font-medium cursor-pointer tracking-tight text-sm">
                            <li onMouseOver={() => setOpen(true)} className="relative px-3 py-2 hover:bg-gray-100 hover:z-50">about
                                <ul className={`absolute text-xs left-0 w-40 mt-2 text-black bg-white ${open ? "block" : "hidden"
                                        }`}>
                                    <li className="flex w-full items-center p-3 hover:bg-gray-700 hover:text-white">
                                        History
                                    </li>
                                    <li className="flex w-full items-center p-3 hover:bg-gray-700 hover:text-white">
                                        Vision Mission
                                    </li>
                                </ul>
                            </li>
                            <li className="px-3 py-2 hover:bg-gray-100">Our Work</li>
                            <li className="px-3 py-2 hover:bg-gray-100">Our Team</li>
                            <li className="px-3 py-2 hover:bg-gray-100">Contact</li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    );
}
export default Navbar;