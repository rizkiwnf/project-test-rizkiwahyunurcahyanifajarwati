const OurValue = () => {
    return (
        <div className="my-[5%]">
            <div className=" text-center font-roboto mb-8 font-extrabold text-xl uppercase tracking-loose">
                Our Values
            </div>
            <div className="mx-44 text-center flex space-x-7">
                <div className="relative p-5 w-[35%] bg-red-400 border-[3px] border-red-500 text-white text-center">
                    <img src="./assets/our-value/light.png" className="w-3 m-auto block" alt="" />
                    <div className="uppercase font-bold text-lg pt-3">
                        innovation
                    </div>
                    <div className="text-sm font-medium py-6">
                        lorem ipsum dolor sit amer, consectetur adipisicing elit. Maxime exercitationem dolorem deserunt, unde, eaque ipsa?
                    </div>
                    <div className="absolute right-[-4.5%] top-[50%] w-0 h-0 border-t-[12px] border-t-white border-l-[15px] border-l-red-500 border-b-[12px] border-b-white">

                    </div>
                </div>

                <div className="relative p-5 w-[35%] bg-green-400 border-[3px] border-green-500 text-white text-center ">
                    <img src="./assets/our-value/bank.png" className="w-5 m-auto block" alt="" />
                    <div className="uppercase font-bold text-lg pt-3">
                        loyalty
                    </div>
                    <div className="text-sm font-medium py-6">
                        lorem ipsum dolor sit amer, consectetur adipisicing elit. Impedit similique eum itaque facere temporibus dolores
                    </div>
                    <div className="absolute right-[-4.5%] top-[50%] w-0 h-0 border-t-[12px] border-t-white border-l-[15px] border-l-green-500 border-b-[12px] border-b-white"></div>
                </div>
                <div className="p-5 w-[35%] bg-blue-400 border-[3px] border-blue-500 text-white text-center ">
                    <img src="./assets/our-value/balance.png" className="w-5 m-auto block" alt="" />
                    <div className="uppercase font-bold text-lg pt-3">
                        balance
                    </div>
                    <div className="text-sm font-medium py-6">
                        lorem ipsum dolor sit amer, consectetur adipisicing elit. Impedit similique eum itaque facere temporibus dolores
                    </div>
                </div>
            </div>
        </div>
    )
}
export default OurValue;