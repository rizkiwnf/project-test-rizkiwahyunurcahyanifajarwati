import { useState } from "react";
import { MdChevronLeft, MdChevronRight } from 'react-icons/md'
import { RxDotFilled } from 'react-icons/rx'

function Home() {
    const listImage = [
        {
            id: 1,
            image: "./assets/carousel/bg.jpg",
            description: "this is a place where technology & creativity fused into digital chemistry",
        },
        {
            id: 2,
            image: "./assets/carousel/bg-2.jpg",
            description: "we don't have the best but we have the greatest team",
        },
    ];

    const [currentIndex, setCurrentIndex, text] = useState(0)

    const prevSlide = () => {
        const isFirstSlide = currentIndex === 0;
        const newIndex = isFirstSlide ? listImage.length - 1 : currentIndex - 1;
        setCurrentIndex(newIndex);
    }
    const nextSlide = () => {
        const isLastSlide = currentIndex === listImage.length - 1;
        const newIndex = isLastSlide ? 0 : currentIndex + 1;
        setCurrentIndex(newIndex);
    }

    const moveSlide = (imageIndex) => {
        setCurrentIndex(imageIndex);
    }

    return (
        <div id="home" className="h-[42rem] w-full m-auto relative group">
            <div style={{ backgroundImage: `url(${listImage[currentIndex].image})` }} className="w-full h-full bg-center bg-cover duration-500">
                {/* text */}
                <div className="absolute bg-black/60 w-[45%] uppercase font-roboto text-2xl p-5 leading-relaxed font-medium text-white bottom-[22%] left-[15%]">
                    <div class="grid grid-cols-4 ">
                        <div class="col-start-1 col-span-4">{listImage[currentIndex].description}</div>
                    </div>
                </div>
                {/* left arrow */}
                <div className=" absolute top-[50%] -translate-x-0 translate-y-[-50%] left-5 text-2xl rounded-full p-2 bg-black/20 text-white cursor-pointer border-white border-2 font-light ">
                    <MdChevronLeft onClick={prevSlide} size={20} />
                </div>
                {/* right arrow */}
                <div className=" absolute top-[50%] -translate-x-0 translate-y-[-50%] right-5 text-2xl rounded-full p-2 bg-black/20 text-white cursor-pointer border-white border-2 font-light">
                    <MdChevronRight onClick={nextSlide} size={20} />
                </div>
                {/* dot icon */}
                <div className="absolute flex inset-x-0 bottom-0 justify-center py-2">
                    {listImage.map((image, imageIndex) => (
                        <div key={imageIndex} onClick={() => moveSlide(imageIndex)} className={`cursor-pointer transition-all w-2 h-2 mx-1 bg-white rounded-full ${currentIndex === imageIndex ? "p-1" : "bg-opacity-50"}`}>
                        </div>
                    ))}
                </div>
            </div>


        </div>
    )
}
export default Home;