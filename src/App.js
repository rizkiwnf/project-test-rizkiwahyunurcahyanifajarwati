import './App.css';
import React from 'react';
import Navbar from './layout/navbar/navbar';
import Home from './layout/homepage/home';
import Contact from './layout/contactus/contact';
import Footer from './layout/footer/footer';

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import OurValue from './layout/ourvalue/ourvalue';



function App() {
  return (
    <React.Fragment>
      <Navbar />  
      <Home />
      <OurValue />
      <Contact />
      <Footer />
    </React.Fragment>
  );
}

export default App;
